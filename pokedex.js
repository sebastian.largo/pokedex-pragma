class pokemon {
    constructor (name,img,no,level,type,typeImg,hability,height,weight) {
        this.name = name;
        this.img = img;
        this.no = no;
        this.level = level;
        this.type = type;
        this.typeImg = typeImg;
        this.hability = hability;
        this.height = height;
        this.weight = weight;
    }
}

let Pokemons = new Array();

Pokemons[0] = charizard = new pokemon('charizard','Charizard.webp','006','100','FIRE, FLYING','GO_Fire.webp','FLAMES','1.7 m','90.5 Kg');
Pokemons[1] = squirtle = new pokemon('squirtle','Squirtle.png','007','77','WATER','GO_Water.webp','TORRENT','0.5 m','9 Kg');
Pokemons[2] = mewTwo = new pokemon('mewTwo','MewTwo.png','150','22','PHYSIC, FIGHTING','GO_Psychic.webp','PRESSURE','2 m','122 Kg');
Pokemons[3] = solgaleo = new pokemon('solgaleo','Solgaleo.webp','791','43','PHYSIC, STEEL','GO_Steel.webp','METAL BODY','3.6 m','230 Kg');
Pokemons[4] = snorlax = new pokemon('snorlax','Snorlax.png','143','69','NORMAL','GO_Normal.webp','GLUTTONY','2.1 m','460 Kg');

let pokemonTypeImg = document.getElementById("pokemonTypeImg");
let pokemonName = document.getElementById("pokemonName");
let pokemonImg = document.getElementById("pokemonImg");
let pokemonNo = document.getElementById("pokemonNo");
let pokemonLevel = document.getElementById("pokemonLevel");
let pokemonType = document.getElementById("pokemonType");
let pokemonHability = document.getElementById("pokemonHability");
let pokemonHeight = document.getElementById("pokemonHeight");
let pokemonWeight = document.getElementById("pokemonWeight");

function chooseAPokemon(newPokemon) {
    let myPokemon = Pokemons[newPokemon];
    pokemonTypeImg.src = `assets/types/${myPokemon.typeImg}`;
    pokemonName.textContent = `${myPokemon.name}`;
    pokemonImg.src = `assets/${myPokemon.img}`;
    pokemonNo.textContent = `${myPokemon.no}`;
    pokemonLevel.textContent = `${myPokemon.level}`;
    pokemonType.textContent = `${myPokemon.type}`;
    pokemonHability.textContent = `${myPokemon.hability}`;
    pokemonHeight.textContent = `${myPokemon.height}`;
    pokemonWeight.textContent = `${myPokemon.weight}`;
}

